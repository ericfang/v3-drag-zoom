# V3DragZoom 

一个基于 `vue3` 开发的轻量、高效缩放拖拽组件，方便开发者快速实现缩放拖拽功能。

## [在线演示和使用手册(DEMO)](https://fangjc1986.github.io/vue3-blog-vitepress/frontend-components/v3-drag-zoom.html)

## GIT地址

[https://gitee.com/ericfang/v3-drag-zoom](https://gitee.com/ericfang/v3-drag-zoom)

## 安装

使用以下命令安装 `v3-drag-zoom`：

```bash
npm install v3-drag-zoom
# 或
yarn add v3-drag-zoom
```

## 导入组件

### 全局导入

在 `main.js` 中全局引入 `v3-drag-zoom`：

```ts
import {createApp} from "vue";
import App from "./App.vue";
// v3-drag-zoom 组件
import V3DragZoom from "v3-drag-zoom";
// v3-drag-zoom 全局样式（必须导入，否则无法正常使用）
import "v3-drag-zoom/dist/style.css";

createApp(App).use(V3DragZoom).mount("#app");
```

### 按需导入

在需要的组件中导入 `v3-drag-zoom`：

```vue
<script setup lang="ts">
  import {V3DragZoomContainer} from "v3-drag-zoom";
</script>

<template>
  <v3-drag-zoom-container>
    <div>需要缩放与拖拽的元素</div>
  </v3-drag-zoom-container>
</template>
```



## API参数

### v3-drag-zoom-container

#### Props

| 参数名             | 类型      | 默认值     | 说明                                    |
|-----------------|---------|---------|---------------------------------------|
| align           | String  | contain | 内容对齐方式，可选值有 `auto`、 `contain`、`cover` |
| autoResize      | Boolean | true    | 是否自动重置尺寸，当容器尺寸为百分比的时候，会根据父容器变化而自动变化   |
| followPointer   | Boolean | true    | 缩放时是否跟随鼠标                             |
| maxZoom         | Float   | 100     | 最大缩放倍数                                |
| minZoom         | Float   | 0.01    | 最小缩放倍数                                |   
| zoomFactor      | Float   | 0.1     | 鼠标滚轮一次的缩放比例                           |
| loading         | Boolean | false   | 是否加载中                                 |
| animateDuration | Number  | 200     | 缩放时候的过度动画时长，单位 ms                     |

#### Slots

| Slot    | 说明          |
|---------|-------------|
| default | 直接填写需要放置的内容 |

#### Exposed

| 参数名   | 类型                   | 说明               |
|-------|----------------------|------------------|
| zoom  | (zoom:Float) => void | 手动缩放 zoom: 为缩放倍数 |
| reset | () => void           | 重置缩放             |

### v3-drag-zoom-item

#### Props

| 参数名               | 类型                          | 默认值         | 是否必须 | 说明                                                 |
|-------------------|-----------------------------|-------------|------|----------------------------------------------------|
| offset            | Array\<Integer\>            | \[-50,-50\] | 否    | 偏移量，默认值代表横向和纵向均偏移 -50%，也就是对齐中心点位置，偏移量单位为 %, 不支持 px |
| fixedSize         | Boolean                     | false       | 否    | 是否固定大小， true代表在缩放过程中该item内容尺寸不变                    |
| rotate            | Float                       | 0           | 否    | 旋转角度单位 deg (360度）                                  |
| draggable         | Boolean                     | false       | 否    | 是否可以拖拽移动                                           |  
| position(v-model) | [CurPosition](#CurPosition) | 无           | 是    | 该 item 在内容中的位置（百分比位置）                              |

#### Slots

| Slot    | 说明          |
|---------|-------------|
| default | 直接填写需要放置的内容 |

#### Events

| 事件名             | 参数                      | 说明                        |
|-----------------|-------------------------|---------------------------|
| onMove          | ( pos :Position)=> void | 每移动一点距离触发，返回当前位置          |
| onMoveFinished  | ( pos :Position)=> void | 移动结束（鼠标抬起或超出范围）触发， 返回当前位置 |

### CurPosition

| 参数名 | 类型    | 默认值 | 是否必须 | 说明     |
|-----|-------|-----|------|--------|
| x   | Float | 无   | 是    | 横向位置 % |
| y   | Float | 无   | 是    | 纵向位置 % |

### Position extend CurPosition

| 参数名 | 类型                       | 默认值 | 是否必须 | 说明                                                                      |
|-----|--------------------------|-----|------|-------------------------------------------------------------------------|
| x   | Float                    | 无   | 是    | 横向位置 %                                                                  |
| y   | Float                    | 无   | 是    | 纵向位置 %                                                                  |
| sub | (pos:Position)=>Position | 无   | 否    | 计算当前 Position 与 pos 之间的差值 new Position(this.x - pos.x, this.y - pos.y); |
| add | (pos:Position)=>Position | 无   | 否    | 计算当前 Position 与 pos 之间的和值 new Position(this.x + pos.x, this.y + pos.y); |



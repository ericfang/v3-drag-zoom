import { CurPosition } from "../v3-drag-zoom-container/v3-drag-zoom-container.support.ts";

export declare type ItemAlign = Array<number>;
export interface V3DragItemProps {
  // 偏移量百分比，[x, y]
  // 默认 [-50,-50] 居中
  offset?: ItemAlign;
  // 是否固定大小，不随缩放而变化
  // 默认 false
  fixedSize?: boolean;
  // 旋转角度 0-360度
  rotate?: number;
  // 是否可拖拽，默认 false
  draggable?: boolean;
  // 默认所在位置（相对于容器的百分比）
  // 在内容层的位置，{x, y}
  position: CurPosition;
}

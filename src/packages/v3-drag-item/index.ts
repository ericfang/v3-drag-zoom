import V3DragZoomItem from "./v3-drag-zoom-item.vue";
import { App } from "vue";

V3DragZoomItem.install = (app: App) => {
  app.component("V3DragZoomItem", V3DragZoomItem);
};
export default V3DragZoomItem;

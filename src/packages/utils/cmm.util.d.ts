import { Position } from "../v3-drag-zoom-container/v3-drag-zoom-container.support.ts";

/**
 * 获取元素的宽高
 * @param el 元素
 */
export declare const getHtmlElementSize: (el: HTMLElement) => number[];
/**
 * 获取鼠标在元素上的位置
 * @param e 鼠标事件
 * @param el 元素
 */
export declare const getMousePositionInElement: (
  e: MouseEvent | WheelEvent,
  el: HTMLElement,
) => Position;
/**
 * 一个简单的 sleep 函数
 * @param ms 毫秒
 */
export declare const sleep: (ms: number) => Promise<any>;

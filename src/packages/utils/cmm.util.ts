import { Position } from "../v3-drag-zoom-container/v3-drag-zoom-container.support.ts";

/**
 * 获取元素的宽高
 * @param el 元素
 */
export const getHtmlElementSize = (el: HTMLElement) => {
  return [el.offsetWidth, el.offsetHeight];
};

/**
 * 获取鼠标在元素上的位置
 * @param e 鼠标事件
 * @param el 元素
 */
export const getMousePositionInElement = (e: MouseEvent | WheelEvent, el: HTMLElement) => {
  const rect = el.getBoundingClientRect();
  return new Position(e.clientX - rect.left, e.clientY - rect.top);
};

/**
 * 一个简单的 sleep 函数
 * @param ms 毫秒
 */
export const sleep = async (ms: number) => new Promise<any>((resolve) => setTimeout(resolve, ms));

import { sleep } from "../utils/cmm.util.ts";

import { ComputedRef, Ref } from "vue";

export interface V3DragZoomProps {
  // 根据父元素自动调整大小，默认true
  // 当容器样式高宽设置为百分比时，会根据父元素的大小自动调整大小
  autoResize?: boolean;
  // 缩放时是否跟随鼠标，默认 true
  followPointer?: boolean;
  // 最大缩放比例，默认 100
  maxZoom?: number;
  // 最小缩放比例，默认 0.01
  minZoom?: number;
  // 显示模式，默认 contain
  align?: AlignMode;
  // 缩放因子 默认0.1
  // 每次滚轮都将放大或缩小当前大小的 zoomFactor 倍
  zoomFactor?: number;
  // 是否可拖拽，默认 true
  draggable?: boolean;
  // 是否可缩放，默认 true
  zoomable?: boolean;
  // 加载中
  loading?: boolean;
  // 缩放动画时长，默认 200 毫秒
  animateDuration?: number;
}

/**
 * 显示模式
 * cover：背景图像尽可能地填充容器，保持图像的纵横比并裁剪超出容器的部分。
 * contain：背景图像尽可能大地显示在容器中，同时保持其纵横比并留有空白。
 * auto：默认值。背景图像尺寸不会被改变，背景图像被放置在容器的默认位置（居中）。
 */
export declare type AlignMode = "cover" | "contain" | "auto";

/**
 * v3DragZoom 的暴露方法
 */
export interface V3DragZoomExpose {
  /**
   * 放大
   * @param zoom 放大的倍数
   */
  zoom: (zoom: number) => void;
  /**
   * 重置图片，恢复到初始位置与大小
   */
  reset: () => void;
}

/**
 * v3DragZoom 的 provide
 * v3-drag-zoom-item 会用到
 */
export interface V3DragZoomProvideItem {
  contentZoom: Ref<number>;
  currentContentSize: { width: Ref<number>; height: Ref<number> };
  orgContentSize: { width: Ref<number>; height: Ref<number> };
  addDragEvent: (event: DragMouseEvents) => () => void;
  removeDragEvent: (event: DragMouseEvents) => void;
  zoomProps: ComputedRef<V3DragZoomProps>;
}

export interface CurPosition {
  x: number;
  y: number;
}

/**
 * 鼠标位置
 */
export class Position implements CurPosition {
  static fromCurPosition(curPosition: CurPosition) {
    return new Position(curPosition.x, curPosition.y);
  }

  constructor(x: number, y: number) {
    this.x = x;
    this.y = y;
  }
  x: number;
  y: number;

  sub(p: Position) {
    return new Position(this.x - p.x, this.y - p.y);
  }

  add(p: Position) {
    return new Position(this.x + p.x, this.y + p.y);
  }
}

/**
 * 加载某个 element 元素下的所有图片
 * 并等待图片加载完毕
 * @param el 元素
 * @param timeout 超时时间
 */
export const loadedAllImages = async (el: HTMLElement, timeout = 5000) => {
  try {
    const images = el.querySelectorAll("img");
    const promises = Array.from(images).map((img) => {
      img.draggable = false;
      return new Promise((resolve) => {
        img.onload = () => {
          resolve(true);
        };
      });
    });
    const timer = setTimeout(() => {
      throw new Error("图片加载超时");
    }, timeout);
    await Promise.all(promises);
    clearTimeout(timer);
    // 这里等待100毫秒，是为了防止图片加载完毕后，还没有渲染到页面上;
    // 如果不等待，会导内容层的宽高计算错误，从而导致图片显示不正确
    if (promises.length) await sleep(100);
  } catch (e) {
    console.error(e);
  }
};

/**
 * 拖拽鼠标事件
 */
export class DragMouseEvents {
  onMousedown: (e: MouseEvent) => void = () => {};
  onMousemove: (e: MouseEvent, diff: Position, diffContentPercent: Position) => void = () => {};
  onMouseup: (e: MouseEvent) => void = () => {};
  onMouseleave: (e: MouseEvent) => void = () => {};
  onMousewheel: (e: WheelEvent, factor: number, zoom: number) => void = () => {};
}

import V3DragZoomContainer from "./v3-drag-zoom-container.vue";
import { App } from "vue";

V3DragZoomContainer.install = (app: App) => {
  app.component("V3DragZoomContainer", V3DragZoomContainer);
};
export default V3DragZoomContainer;

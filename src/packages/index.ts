import { App } from "vue";
import V3DragZoomContainer from "./v3-drag-zoom-container/v3-drag-zoom-container.vue";
import V3DragZoomItem from "./v3-drag-item/v3-drag-zoom-item.vue";
import { Position } from "./v3-drag-zoom-container/v3-drag-zoom-container.support.ts";

export default {
  install: (app: App): any => {
    app.component("V3DragZoomContainer", V3DragZoomContainer);
    app.component("V3DragZoomItem", V3DragZoomItem);
  },
};
export { V3DragZoomContainer, V3DragZoomItem, Position };

declare module "vue" {
  export interface GlobalComponents {
    V3DragZoomContainer: typeof V3DragZoomContainer;
    V3DragZoomItem: typeof V3DragZoomItem;
  }
}

import { createApp } from "vue";
import App from "./App.vue";
import v3DragZoom from "./packages/index.ts";
import ArcoVue from "@arco-design/web-vue";
import "@arco-design/web-vue/dist/arco.less";

createApp(App).use(ArcoVue).use(v3DragZoom).mount("#app");
